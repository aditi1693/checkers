import random
import time

import pygame, sys
from pygame.locals import *
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np

pygame.font.init()

##COLORS##
#             R    G    B
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
RED = (255, 0, 0)
BLACK = (0, 0, 0)
GOLD = (255, 215, 0)
HIGH = (160, 190, 255)

##DIRECTIONS##
NORTHWEST = "northwest"
NORTHEAST = "northeast"
SOUTHWEST = "southwest"
SOUTHEAST = "southeast"


class Game:
    """
    The main game control.
    """

    def __init__(self):
        self.graphics = Graphics()
        self.board = Board()

        self.turn = BLUE
        self.selected_piece = None  # a board location.
        self.hop = False
        self.selected_legal_moves = []
        self.block = False
        self.active_pieces = []
        self.action = None
        self.matrix1 = self.board.new_board()

    def setup(self):
        """Draws the window and board at the beginning of the game"""
        self.graphics.setup_window()

    def event_loop(self):
        """
        The event loop. This is where events are triggered
        (like a mouse click) and then effect the game state.
        """

        self.mouse_pos = self.graphics.board_coords(pygame.mouse.get_pos())  # what square is the mouse in?.action
        active_pieces = self.board.activePieces(self.turn)
        print(active_pieces)
        self.action = random.choices(active_pieces)
        # print(self.action[0])
        if self.selected_piece != None:
            self.selected_legal_moves = self.board.legal_moves(self.selected_piece, self.hop)

        for event in pygame.event.get():

            if event.type == QUIT:
                self.terminate_game()

            if event.type == MOUSEBUTTONDOWN:
                if self.hop == False:
                    if self.board.location(self.mouse_pos).occupant != None and self.board.location(
                            self.mouse_pos).occupant.color == self.turn:
                        self.selected_piece = self.mouse_pos

                    elif self.selected_piece != None and self.mouse_pos in self.board.legal_moves(self.selected_piece):

                        self.board.move_piece(self.selected_piece, self.mouse_pos)

                        if self.mouse_pos not in self.board.adjacent(self.selected_piece):
                            self.board.remove_piece((self.selected_piece[0] + (
                                    self.mouse_pos[0] - self.selected_piece[0]) // 2, self.selected_piece[1] + (
                                                             self.mouse_pos[1] - self.selected_piece[1]) // 2))

                            self.hop = True
                            self.selected_piece = self.mouse_pos

                        else:
                            self.end_turn()

                if self.hop == True:
                    if self.selected_piece != None and self.mouse_pos in self.board.legal_moves(self.selected_piece,
                                                                                                self.hop):
                        self.board.move_piece(self.selected_piece, self.mouse_pos)
                        self.board.remove_piece((self.selected_piece[0] + (
                                self.mouse_pos[0] - self.selected_piece[0]) // 2, self.selected_piece[1] + (
                                                         self.mouse_pos[1] - self.selected_piece[1]) // 2))

                    if self.board.legal_moves(self.mouse_pos, self.hop) == []:
                        self.end_turn()

                    else:
                        self.selected_piece = self.mouse_pos
        self.matrix1 = self.board.getMatrix()
        # print(self.board.return_matrix())

    def boardMovement(self):
        self.board.setMatrix(self.matrix1)
        if self.selected_piece != None:
            self.selected_legal_moves = self.board.legal_moves(self.selected_piece, self.hop)

        if self.hop == False:

            if self.selected_piece != None and self.action in self.board.legal_moves(self.selected_piece):

                self.board.move_piece(self.selected_piece, self.action)

                if self.action not in self.board.adjacent(self.selected_piece):
                    self.board.remove_piece((self.selected_piece[0] + (
                            self.action[0] - self.selected_piece[0]) // 2, self.selected_piece[1] + (
                                                     self.action[1] - self.selected_piece[1]) // 2))

                    self.hop = True
                    self.selected_piece = self.action

                else:
                    self.end_turn()

        if self.hop == True:
            action = self.board.legal_moves(self.selected_piece, self.hop)

            if action != [] and action[0] not in self.board.adjacent(self.selected_piece):
                if self.selected_piece != None and action[0] in self.board.legal_moves(self.selected_piece,
                                                                                       self.hop):
                    self.board.move_piece(self.selected_piece, action[0])
                    self.board.remove_piece((self.selected_piece[0] + (
                            action[0][0] - self.selected_piece[0]) // 2, self.selected_piece[1] + (
                                                     action[0][1] - self.selected_piece[1]) // 2))
                    self.end_turn()
            else:
                self.end_turn()

            # else:
            #     self.selected_piece = action[0]
        self.update()
        self.board.setMatrix(self.board.getMatrix())
        self.matrix1 = self.board.getMatrix()
        # print(self.board.return_matrix())
        return self.board.return_matrix()

    def update(self):
        """Calls on the graphics class to update the game display."""
        self.graphics.update_display(self.board, self.selected_legal_moves, self.selected_piece)

    def terminate_game(self):
        """Quits the program and ends the game."""
        pygame.quit()
        sys.exit()

    def set_selectedPiece(self, x, y):
        self.selected_piece = x, y

    def set_action(self, x, y):
        self.action = x, y

    def set_matrix(self, matrix):
        self.matrix1 = matrix

    def main(self):
        """"This executes the game and controls its flow."""
        self.setup()

        while True:  # main game loop
            self.event_loop()
            self.update()

    def return_matrix1(self):

        return self.matrix1

    def return_matrix(self):
        i = 0
        j = 0
        newmatrix = [[0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0]
            , [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0]]
        matrix = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
                  [0, 0, 0, 0]]
        for x in range(8):
            for y in range(8):
                if self.matrix1[x][y].occupant != None:
                    if self.matrix1[x][y].occupant.king:
                        if self.matrix1[x][y].occupant.color == (0, 0, 255):
                            newmatrix[y][x] = 1
                        else:
                            newmatrix[y][x] = -1
                    else:
                        if self.matrix1[x][y].occupant.color == (0, 0, 255):
                            newmatrix[y][x] = 2
                        else:
                            newmatrix[y][x] = -2
                else:
                    newmatrix[y][x] = 0
        i = 0
        for x in range(8):
            j = 0
            for y in range(8):
                if self.matrix1[x][y].color == BLACK:
                    matrix[x][j] = newmatrix[x][y]
                    j = j + 1

        return matrix

    def flip(self, matrix):
        for x in range(8):
            for y in range(4):
                matrix[x][y] = matrix[x][y] * -1
        return matrix

    def end_turn(self):
        """
        End the turn. Switches the current player.
        end_turn() also checks for and game and resets a lot of class attributes.
        """
        if self.turn == BLUE:
            self.turn = RED
        else:
            self.turn = BLUE

        self.selected_piece = None
        self.selected_legal_moves = []
        self.hop = False

        if self.check_for_endgame():
            if self.turn == BLUE:
                self.graphics.draw_message("RED WINS!")

            else:
                self.graphics.draw_message("BLUE WINS!")

    def remove_message(self):
        self.graphics.draw_message("")

    def check_for_endgame(self):
        """
        Checks to see if a player has run out of moves or pieces. If so, then return True. Else return False.
        """
        for x in range(8):
            for y in range(8):
                if self.board.location((x, y)).color == BLACK and self.board.location(
                        (x, y)).occupant != None and self.board.location((x, y)).occupant.color == self.turn:
                    if self.board.legal_moves((x, y)) != []:
                        return False

        return True

    def select_red_move(self):
        position = []
        active_pieces_red = self.board.activePieces((255, 0, 0))
        selected_Piece = random.choices(active_pieces_red)
        (x, y) = selected_Piece[0]

        self.set_selectedPiece(x, y)
        action = random.choice(self.board.legal_moves(self.selected_piece))
        (x1, y1) = action
        self.set_action(x1, y1)
        # print("red selected Piece", self.selected_piece)
        # print("red action", self.action)

        position = self.boardMovement()
        return position

    def check_for_turn(self):
        return self.turn


class Graphics:
    def __init__(self):
        self.caption = "Checkers"

        self.fps = 60
        self.clock = pygame.time.Clock()

        self.window_size = 600
        self.screen = pygame.display.set_mode((self.window_size, self.window_size))
        self.background = pygame.image.load('resources/board.png')

        self.square_size = self.window_size // 8
        self.piece_size = self.square_size // 2
        # print(self.window_size)
        # print(self.square_size)

        self.message = False

    def setup_window(self):
        """
        This initializes the window and sets the caption at the top.
        """
        pygame.init()
        pygame.display.set_caption(self.caption)

    def update_display(self, board, legal_moves, selected_piece):
        """
        This updates the current display.
        """
        self.screen.blit(self.background, (0, 0))

        self.highlight_squares(legal_moves, selected_piece)
        self.draw_board_pieces(board)

        if self.message:
            self.screen.blit(self.text_surface_obj, self.text_rect_obj)

        pygame.display.update()
        self.clock.tick(self.fps)

    def draw_board_squares(self, board):
        """
        Takes a board object and draws all of its squares to the display
        """
        for x in range(8):
            for y in range(8):
                pygame.draw.rect(self.screen, board[x][y].color,
                                 (x * self.square_size, y * self.square_size, self.square_size, self.square_size), )

    def draw_board_pieces(self, board):
        """
        Takes a board object and draws all of its pieces to the display
        """
        for x in range(8):
            for y in range(8):

                if board.matrix[x][y].occupant != None:
                    pygame.draw.circle(self.screen, board.matrix[x][y].occupant.color, self.pixel_coords((x, y)),
                                       int(round(self.piece_size)))

                    if board.location((x, y)).occupant.king == True:
                        pygame.draw.circle(self.screen, GOLD, self.pixel_coords((x, y)), int(self.piece_size / 1.7),
                                           self.piece_size // 4)

    def pixel_coords(self, board_coords):
        """
        Takes in a tuple of board coordinates (x,y)
        and returns the pixel coordinates of the center of the square at that location.
        """
        return (
            board_coords[0] * self.square_size + self.piece_size, board_coords[1] * self.square_size + self.piece_size)

    def board_coords(self, xxx_todo_changeme):
        """
        Does the reverse of pixel_coords(). Takes in a tuple of of pixel coordinates and returns what square they are in.
        """
        (pixel_x, pixel_y) = xxx_todo_changeme
        return (pixel_x // self.square_size, pixel_y // self.square_size)

    def highlight_squares(self, squares, origin):
        """
        Squares is a list of board coordinates.
        highlight_squares highlights them.
        """
        for square in squares:
            pygame.draw.rect(self.screen, HIGH, (
                square[0] * self.square_size, square[1] * self.square_size, self.square_size, self.square_size))

        if origin != None:
            pygame.draw.rect(self.screen, HIGH, (
                origin[0] * self.square_size, origin[1] * self.square_size, self.square_size, self.square_size))

    def draw_message(self, message):
        """
        Draws message to the screen.
        """
        self.message = True
        self.font_obj = pygame.font.Font('freesansbold.ttf', 44)
        self.text_surface_obj = self.font_obj.render(message, True, HIGH, BLACK)
        self.text_rect_obj = self.text_surface_obj.get_rect()
        self.text_rect_obj.center = (self.window_size / 2, self.window_size / 2)


class Board:
    def __init__(self):
        self.matrix = self.new_board()

    def new_board(self):
        """
        Create a new board matrix.
        """

        # initialize squares and place them in matrix

        matrix = [[None] * 8 for i in range(8)]

        # The following code block has been adapted from
        # http://itgirl.dreamhosters.com/itgirlgames/games/Program%20Leaders/ClareR/Checkers/checkers.py
        for x in range(8):
            for y in range(8):
                if (x % 2 != 0) and (y % 2 == 0):
                    matrix[y][x] = Square(BLACK)
                elif (x % 2 != 0) and (y % 2 != 0):
                    matrix[y][x] = Square(WHITE)
                elif (x % 2 == 0) and (y % 2 != 0):
                    matrix[y][x] = Square(BLACK)
                elif (x % 2 == 0) and (y % 2 == 0):
                    matrix[y][x] = Square(WHITE)

        # initialize the pieces and put them in the appropriate squares

        for x in range(8):
            for y in range(3):
                if matrix[x][y].color == BLACK:
                    matrix[x][y].occupant = Piece(RED)
            for y in range(5, 8):
                if matrix[x][y].color == BLACK:
                    matrix[x][y].occupant = Piece(BLUE)

        return matrix

    def set_board(self):
        """
        Create a new board matrix.
        """

        # initialize squares and place them in matrix

        matrix = [[None] * 8 for i in range(8)]

        # The following code block has been adapted from
        # http://itgirl.dreamhosters.com/itgirlgames/games/Program%20Leaders/ClareR/Checkers/checkers.py
        for x in range(8):
            for y in range(8):
                if (x % 2 != 0) and (y % 2 == 0):
                    matrix[y][x] = Square(BLACK)
                elif (x % 2 != 0) and (y % 2 != 0):
                    matrix[y][x] = Square(WHITE)
                elif (x % 2 == 0) and (y % 2 != 0):
                    matrix[y][x] = Square(BLACK)
                elif (x % 2 == 0) and (y % 2 == 0):
                    matrix[y][x] = Square(WHITE)

        # initialize the pieces and put them in the appropriate squares

        redYList = [1, 2]
        redYking = [0, 1, 2]
        blueYList = [5, 6]
        blueYKing = [5, 6, 7]
        XList = [0, 1, 2, 3, 4, 5, 6, 7]
        redPiece = 0
        bluePiece = 0
        redKing = 0
        bluKing = 0

        while redPiece < 3:
            x = random.choice(XList)
            y = random.choice(redYList)
            if matrix[x][y].color == BLACK and matrix[x][y].occupant == None:
                matrix[x][y].occupant = Piece(RED)
                redPiece = redPiece + 1

        while redKing < 3:
            x = random.choice(XList)
            y = random.choice(redYking)
            if matrix[x][y].color == BLACK and matrix[x][y].occupant == None:
                matrix[x][y].occupant = Piece(RED)
                redKing = redKing + 1

        while bluePiece < 3:
            x = random.choice(XList)
            y = random.choice(blueYList)
            if matrix[x][y].color == BLACK and matrix[x][y].occupant == None:
                matrix[x][y].occupant = Piece(BLUE)
                bluePiece = bluePiece + 1

        while bluKing < 3:
            x = random.choice(XList)
            y = random.choice(blueYKing)
            if matrix[x][y].color == BLACK and matrix[x][y].occupant == None:
                matrix[x][y].occupant = Piece(BLUE)
                bluKing = bluKing + 1

        # self.set_new_board()

        return matrix

    def set_new_board(self):
        matrix = self.set_board()
        XList = [0, 1, 2, 3, 4, 5, 6, 7]
        redYking = [0, 1, 2]
        blueYKing = [5, 6, 7]
        blueKing = 0
        redKing = 0

        while redKing < 3:
            x = random.choice(XList)
            y = random.choice(redYking)
            if self.location((x, y)).occupant != None:
                self.location((x, y)).occupant.king = True
                print("RedKing", self.location((x, y)).occupant.king)
                redKing = redKing + 1

        while redKing < 3:
            x = random.choice(XList)
            y = random.choice(blueYKing)
            if self.location((x, y)).occupant != None:
                self.location((x, y)).occupant.king = True
                print("BlueKing", self.location((x, y)).occupant.king)
                blueKing = blueKing + 1

    def return_matrix(self):
        newmatrix = [[0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0]
            , [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0]]
        for x in range(8):
            for y in range(8):
                if self.matrix[x][y].occupant != None:
                    newmatrix[y][x] = self.matrix[x][y].occupant.color

                else:
                    newmatrix[y][x] = 0
        return newmatrix

    def getMatrix(self):
        return self.matrix

    def setMatrix(self, matrix):
        self.matrix = matrix

    def board_string(self, board):
        """
        Takes a board and returns a matrix of the board space colors. Used for testing new_board()
        """

        board_string = [[None] * 8] * 8

        for x in range(8):
            for y in range(8):
                if board[x][y].color == WHITE:
                    board_string[x][y] = "WHITE"
                else:
                    board_string[x][y] = "BLACK"

        return board_string

    def rel(self, dir, xxx_todo_changeme1):
        """
        Returns the coordinates one square in a different direction to (x,y).

        # ===DOCTESTS===
        #
        # >>> board = Board()
        #
        # >>> board.rel(NORTHWEST, (1,2))
        # (0,1)
        #
        # >>> board.rel(SOUTHEAST, (3,4))
        # (4,5)
        #
        # >>> board.rel(NORTHEAST, (3,6))
        # (4,5)
        #
        # >>> board.rel(SOUTHWEST, (2,5))
        # (1,6)
        # """
        (x, y) = xxx_todo_changeme1
        if dir == NORTHWEST:
            return (x - 1, y - 1)
        elif dir == NORTHEAST:
            return (x + 1, y - 1)
        elif dir == SOUTHWEST:
            return (x - 1, y + 1)
        elif dir == SOUTHEAST:
            return (x + 1, y + 1)
        else:
            return 0

    def adjacent(self, xxx_todo_changeme2):
        """
        Returns a list of squares locations that are adjacent (on a diagonal) to (x,y).
        """
        (x, y) = xxx_todo_changeme2
        return [self.rel(NORTHWEST, (x, y)), self.rel(NORTHEAST, (x, y)), self.rel(SOUTHWEST, (x, y)),
                self.rel(SOUTHEAST, (x, y))]

    def location(self, xxx_todo_changeme3):
        """
        Takes a set of coordinates as arguments and returns self.matrix[x][y]
        This can be faster than writing something like self.matrix[coords[0]][coords[1]]
        """
        (x, y) = xxx_todo_changeme3
        return self.matrix[x][y]

    def blind_legal_moves(self, xxx_todo_changeme4):
        """
        Returns a list of blind legal move locations from a set of coordinates (x,y) on the board.
        If that location is empty, then blind_legal_moves() return an empty list.
        """
        (x, y) = xxx_todo_changeme4
        if self.matrix[x][y].occupant != None:

            if self.matrix[x][y].occupant.king == False and self.matrix[x][y].occupant.color == BLUE:
                blind_legal_moves = [self.rel(NORTHWEST, (x, y)), self.rel(NORTHEAST, (x, y))]

            elif self.matrix[x][y].occupant.king == False and self.matrix[x][y].occupant.color == RED:
                blind_legal_moves = [self.rel(SOUTHWEST, (x, y)), self.rel(SOUTHEAST, (x, y))]

            else:
                blind_legal_moves = [self.rel(NORTHWEST, (x, y)), self.rel(NORTHEAST, (x, y)),
                                     self.rel(SOUTHWEST, (x, y)), self.rel(SOUTHEAST, (x, y))]

        else:
            blind_legal_moves = []

        return blind_legal_moves

    def legal_moves(self, xxx_todo_changeme5, hop=False):
        """
        Returns a list of legal move locations from a given set of coordinates (x,y) on the board.
        If that location is empty, then legal_moves() returns an empty list.
        """
        (x, y) = xxx_todo_changeme5
        blind_legal_moves = self.blind_legal_moves((x, y))
        legal_moves = []
        clr = self.location((x, y)).occupant.color
        flag = 0
        status = self.check_for_jumps(clr)

        for move in blind_legal_moves:
            if self.on_board(move) and self.location(move).occupant != None:
                if self.location(move).occupant.color != self.location((x, y)).occupant.color and self.on_board(
                        (move[0] + (move[0] - x), move[1] + (move[1] - y))) and self.location((move[0] + (
                        move[0] - x), move[1] + (
                        move[1] - y))).occupant == None:  # is this location filled by an enemy piece?
                    flag = 1

        if hop == False and flag == 0 and status == False:
            for move in blind_legal_moves:
                if hop == False:
                    if self.on_board(move):
                        if self.location(move).occupant == None:
                            legal_moves.append(move)

                        elif self.location(move).occupant.color != self.location(
                                (x, y)).occupant.color and self.on_board(
                            (move[0] + (move[0] - x), move[1] + (move[1] - y))) and self.location((move[0] + (
                                move[0] - x), move[1] + (
                                move[1] - y))).occupant == None:  # is this location filled by an enemy piece?
                            legal_moves.append((move[0] + (move[0] - x), move[1] + (move[1] - y)))

        else:  # hop == True
            for move in blind_legal_moves:
                if self.on_board(move) and self.location(move).occupant != None:
                    if self.location(move).occupant.color != self.location((x, y)).occupant.color and self.on_board(
                            (move[0] + (move[0] - x), move[1] + (move[1] - y))) and self.location((move[0] + (
                            move[0] - x), move[1] + (
                            move[1] - y))).occupant == None:  # is this location filled by an enemy piece?
                        # legal_moves.clear()
                        legal_moves.append((move[0] + (move[0] - x), move[1] + (move[1] - y)))

        return legal_moves

    def check_for_jumps(self, clr):
        for x in range(8):
            for y in range(8):
                blind_legal_moves = []
                if self.location((x, y)).occupant != None and self.location((x, y)).occupant.color == clr:
                    blind_legal_moves = self.blind_legal_moves((x, y))

                for move in blind_legal_moves:
                    if self.on_board(move) and self.location(move).occupant != None:
                        if self.location(move).occupant.color != self.location((x, y)).occupant.color and self.on_board(
                                (move[0] + (move[0] - x), move[1] + (move[1] - y))) and self.location((move[0] + (
                                move[0] - x), move[1] + (
                                move[1] - y))).occupant == None:  # is this location filled by an enemy piece?
                            return True
        return False

    def activePieces(self, clr):
        active_pieces = []
        for x in range(8):
            for y in range(8):
                if self.location((x, y)).occupant != None and self.location((x, y)).occupant.color == clr:
                    legal_moves = self.legal_moves((x, y))
                    if legal_moves != []:
                        active_pieces.append((x, y))
        return active_pieces

    def remove_piece(self, xxx_todo_changeme6):
        """
        Removes a piece from the board at position (x,y).
        """
        (x, y) = xxx_todo_changeme6
        self.matrix[x][y].occupant = None

    def move_piece(self, xxx_todo_changeme7, xxx_todo_changeme8):
        """
        Move a piece from (start_x, start_y) to (end_x, end_y).
        """
        (start_x, start_y) = xxx_todo_changeme7
        (end_x, end_y) = xxx_todo_changeme8
        self.matrix[end_x][end_y].occupant = self.matrix[start_x][start_y].occupant
        self.remove_piece((start_x, start_y))

        self.king((end_x, end_y))

    def is_end_square(self, coords):
        """
        Is passed a coordinate tuple (x,y), and returns true or
        false depending on if that square on the board is an end square.

        # ===DOCTESTS===
        #
        # >>> board = Board()
        #
        # >>> board.is_end_square((2,7))
        # True
        #
        # >>> board.is_end_square((5,0))
        # True
        #
        # >>>board.is_end_square((0,5))
        # False
        # """

        if coords[1] == 0 or coords[1] == 7:
            return True
        else:
            return False

    def on_board(self, xxx_todo_changeme9):
        """
        Checks to see if the given square (x,y) lies on the board.
        If it does, then on_board() return True. Otherwise it returns false.

        # ===DOCTESTS===
        # >>> board = Board()
        #
        # >>> board.on_board((5,0)):
        # True
        #
        # >>> board.on_board(-2, 0):
        # False
        #
        # >>> board.on_board(3, 9):
        # False
        # """
        (x, y) = xxx_todo_changeme9
        if x < 0 or y < 0 or x > 7 or y > 7:
            return False
        else:
            return True

    def king(self, xxx_todo_changeme10):
        """
        Takes in (x,y), the coordinates of square to be considered for kinging.
        If it meets the criteria, then king() kings the piece in that square and kings it.
        """
        (x, y) = xxx_todo_changeme10
        if self.location((x, y)).occupant != None:
            if (self.location((x, y)).occupant.color == BLUE and y == 0) or (
                    self.location((x, y)).occupant.color == RED and y == 7):
                self.location((x, y)).occupant.king = True


class Piece:
    def __init__(self, color, king=False):
        self.color = color
        self.king = king


class Square:
    def __init__(self, color, occupant=None):
        self.color = color  # color is either BLACK or WHITE
        self.occupant = occupant  # occupant is a Square object


def main():
    game = Game()
    game.main()


if __name__ == "__main__":
    main()


class CheckersEnv:

    def __init__(self):
        self.game = Game()
        self.board = Board()
        self.position = []

        self.action_space = spaces.Discrete(48)
        self.observation_space = spaces.Tuple(
            (spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5),
             spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5),
             spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5),
             spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5),
             spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5),
             spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5), spaces.Discrete(5),
             spaces.Discrete(5), spaces.Discrete(5)))
        # self.observation_space = spaces.Tuple((np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2])
        #                                        ,np.array([-2, -1, 0, 1, 2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2])
        #                                        , np.array([-2, -1, 0, 1, 2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2])
        #                                        , np.array([-2, -1, 0, 1, 2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2])
        #                                        , np.array([-2, -1, 0, 1, 2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2]),np.array([-2,-1,0,1,2])
        #                                        , np.array([-2, -1, 0, 1, 2]),np.array([-2,-1,0,1,2])
        #     ))

        self.seed()
        self.reset()
        self.selected_piece = None
        self.action = None
        self.selected_legal_moves = []
        self.graphics = Graphics()
        self.turn = BLUE
        self.hop = False
        self.done = False

    def info(self, matrix, turn):
        legalMoves = []
        self.board.setMatrix(matrix)
        self.game.set_matrix(matrix)
        active_piece = self.board.activePieces(turn)
        for p in range(len(active_piece)):
            moves = self.board.legal_moves(active_piece[p])
            for x in range(len(moves)):
                legalMoves.append(moves[x])
        return legalMoves

    def return_states(self, matrix, turn):
        stateMatrix = []
        self.board.setMatrix(matrix)
        self.game.set_matrix(matrix)
        active_piece = self.board.activePieces(turn)
        for p in range(len(active_piece)):
            moves = self.board.legal_moves(active_piece[p])
            for x in range(len(moves)):
                selected_piece = active_piece[p]
                action = moves[x]
                self.game.set_selectedPiece(selected_piece[0], selected_piece[1])
                self.game.set_action(action[0], action[1])
                self.position = self.game.boardMovement()
                matrix = self.game.return_matrix()
                stateMatrix.append(matrix)
        return stateMatrix

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):
        assert self.action_space.contains(action[0])
        self.game.setup()

        reward = 0
        notFound = True
        self.board.setMatrix(action[1])
        count = 0
        active_pieces_blue = self.board.activePieces((0, 0, 255))
        active_pieces_red = self.board.activePieces((255, 0, 0))
        self.done = False
        # print("activePiexes",active_pieces_blue)

        for x in range(len(active_pieces_blue)):
            legal_moves = self.board.legal_moves(active_pieces_blue[x])

            for y in range(len(legal_moves)):
                if notFound:
                    count = count + 1

                    if count == action[0] + 1:
                        self.action = legal_moves[y]
                        self.selected_piece = active_pieces_blue[x]
                        # print("selected Piece",self.selected_piece)
                        # print("action",self.action)

                        notFound = False
                        break
                    else:
                        notFound = True

        if notFound == False:
            self.game.set_selectedPiece(self.selected_piece[0], self.selected_piece[1])
            self.game.set_action(self.action[0], self.action[1])
            time.sleep(1)
            self.position = self.game.boardMovement()
            self.done = False
            if self.game.check_for_turn() == (255, 0, 0) and self.game.check_for_endgame() is False:
                time.sleep(1)
                self.position = self.game.select_red_move()

        blue_active_list = self.board.activePieces((0, 0, 255))
        red_active_list = self.board.activePieces((255, 0, 0))
        if not blue_active_list:
            self.done = True
            reward = -1
            # print(reward)
        else:
            if not red_active_list:
                self.done = True
                reward = 1
                # print(reward)

        self.game.remove_message()
        turn = self.game.check_for_turn()

        return self._get_obs(), reward, self.done, {}, turn

    def stepBlue(self, action):
        assert self.action_space.contains(action[0])
        self.game.setup()

        reward = 0
        notFound = True
        self.board.setMatrix(action[1])
        count = 0
        active_pieces_blue = self.board.activePieces((0, 0, 255))
        active_pieces_red = self.board.activePieces((255, 0, 0))
        self.done = False
        # print("activePiexes",active_pieces_blue)

        for x in range(len(active_pieces_blue)):
            legal_moves = self.board.legal_moves(active_pieces_blue[x])

            for y in range(len(legal_moves)):
                if notFound:
                    count = count + 1

                    if count == action[0] + 1:
                        self.action = legal_moves[y]
                        self.selected_piece = active_pieces_blue[x]
                        # print("selected Piece",self.selected_piece)
                        # print("action",self.action)

                        notFound = False
                        break
                    else:
                        notFound = True

        if notFound == False:
            self.game.set_selectedPiece(self.selected_piece[0], self.selected_piece[1])
            self.game.set_action(self.action[0], self.action[1])
            time.sleep(2)
            self.position = self.game.boardMovement()
            self.done = False

        blue_active_list = self.board.activePieces((0, 0, 255))
        red_active_list = self.board.activePieces((255, 0, 0))
        if self.game.check_for_endgame():
            self.done = True
            reward = 1

        self.game.remove_message()

        turn = self.game.check_for_turn()

        return self._get_obs(), reward, self.done, {}, turn

    def stepRed(self, action):
        assert self.action_space.contains(action[0])
        self.game.setup()

        reward = 0
        notFound = True
        self.board.setMatrix(action[1])
        count = 0
        active_pieces_blue = self.board.activePieces((0, 0, 255))
        active_pieces_red = self.board.activePieces((255, 0, 0))
        self.done = False
        # print("activePiexes",active_pieces_blue)

        for x in range(len(active_pieces_red)):
            legal_moves = self.board.legal_moves(active_pieces_red[x])

            for y in range(len(legal_moves)):
                if notFound:
                    count = count + 1

                    if count == action[0] + 1:
                        self.action = legal_moves[y]
                        self.selected_piece = active_pieces_red[x]
                        # print("selected Piece",self.selected_piece)
                        # print("action",self.action)

                        notFound = False
                        break
                    else:
                        notFound = True

        if notFound == False:
            self.game.set_selectedPiece(self.selected_piece[0], self.selected_piece[1])
            self.game.set_action(self.action[0], self.action[1])
            time.sleep(2)
            self.position = self.game.boardMovement()
            self.done = False

        blue_active_list = self.board.activePieces((0, 0, 255))
        red_active_list = self.board.activePieces((255, 0, 0))
        if self.game.check_for_endgame():
            self.done = True
            reward = 1

        self.game.remove_message()

        turn = self.game.check_for_turn()

        matrix1, matrix = self._get_obs()
        newmatrix = self.game.flip(matrix)
        matrix = matrix1, newmatrix

        return matrix, reward, self.done, {}, turn

    def _get_obs(self):
        # self.position=self.board.return_matrix()
        self.board.setMatrix(self.game.return_matrix1())
        # print(self.game.return_matrix())
        return self.game.return_matrix1(), self.game.return_matrix()

    # def reset(self):
    #     self.board=Board()
    #     self.board.setMatrix(self.board.set_board())
    #     self.game.set_matrix(self.board.set_board())
    #     # print(self.game.return_matrix())
    #     # print(self.board.return_matrix())
    #     return  self.game.return_matrix1(),self.game.return_matrix()

    def reset(self):
        self.board = Board()
        self.board.setMatrix(self.board.new_board())
        self.game.set_matrix(self.board.new_board())
        # print(self.game.return_matrix())
        # print(self.board.return_matrix())
        return self.game.return_matrix1(), self.game.return_matrix()
