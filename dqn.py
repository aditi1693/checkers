import random
import gym
import numpy as np
import tensorflow as tf
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from checkers_env import *
import matplotlib.pyplot
from matplotlib import pyplot as plt

import os
from keras.models import load_model

# from scores.score_logger import ScoreLogger
# from scores.score_logger import ScoreLogger

print("tensorflow version", tf.__version__)

ENV_NAME = "CartPole-v1"
checkpoint_fname = "checkpoint.h5"

GAMMA = 0.95
LEARNING_RATE = 0.001

MEMORY_SIZE = 10000000
BATCH_SIZE = 60

EXPLORATION_MAX = 1.0
EXPLORATION_MIN = 0.01
EXPLORATION_DECAY = 0.995


class DQNSolver:

    def __init__(self, observation_space, action_space):
        self.exploration_rate = EXPLORATION_MAX
        # self.exploration_rate = 0

        self.action_space = action_space
        self.memory = deque(maxlen=MEMORY_SIZE)

        self.model = Sequential()
        self.model.add(Dense(32, input_shape=(observation_space,), activation="relu"))
        self.model.add(Dense(32, activation="relu"))
        self.model.add(Dense(1, activation="linear"))
        self.model.compile(loss="mse", optimizer=Adam(lr=LEARNING_RATE))

    # def setActionSpace(self,action_space):
    #     self.action_space=action_space

    def remember(self, state, reward):
        # self.memory.append((state, action, reward, next_state, done))
        self.memory.append((state, reward))

    def act(self, state, matrix, turn, ep):
        env = CheckersEnv()
        actionList = env.info(matrix, turn)
        actions = len(actionList)
        v_value = []
        # FLIP
        # generTE all actions in actionList
        if ep==False:
            self.exploration_rate = 0


        if np.random.rand() < self.exploration_rate:
            return random.randrange(actions)  # pick out of the action list rangomly

        # for action in range(len(actionList)):
        for nextState in range(len(actionList)):
            action = nextState, matrix
            if turn == (0, 0, 255):
                state_next, reward, terminal, info, turn = env.stepBlue(action)
            else:
                state_next, reward, terminal, info, turn = env.stepRed(action)
            nextState = np.reshape(state_next[1], [1, 32])
            v_value.append(self.model.predict(nextState))
        arg = np.argmax(v_value[0])
        return arg

    # loop through each action and generate each next state
    #  loop through each next state and call model predict
    # arg max to find the action to take

    def experience_replay(self, decay):
        if len(self.memory) < BATCH_SIZE:
            return
        batch = random.sample(self.memory, BATCH_SIZE)
        for state, reward in batch:
            v_value = self.model.predict(state)
            v_update = LEARNING_RATE * (reward - v_value)

            self.model.fit(state, v_update, verbose=0)
        if decay:
            self.exploration_rate *= EXPLORATION_DECAY
            self.exploration_rate = max(EXPLORATION_MIN, self.exploration_rate)


def checkers():
    redWin = 0
    red = []
    blue = []
    game = []
    steps = []
    results = []
    blueWin = 0
    env = CheckersEnv()
    observation_space = 32
    action_space = env.action_space.n
    dqn_solver = DQNSolver(observation_space, action_space)

    if os.path.exists(checkpoint_fname):
        dqn_solver.model.load_weights(checkpoint_fname)
        print("Restored")
    else:
        print("Training fresh model")


    run = 0
    # while run < 1000:
    #     run += 1
    #     game.append(run)
    #     state = env.reset()
    #     matrix = state[0]
    #     state = np.reshape(state[1], [1, observation_space])
    #     step = 0
    #     turn = (0, 0, 255)
    #
    #     while True:
    #         step += 1
    #         # env.render()
    #         ep = True
    #         act1 = dqn_solver.act(state, matrix, turn, ep)
    #         action = act1, matrix
    #         if turn == (0, 0, 255):
    #             state_next, reward, terminal, info, turn = env.stepBlue(action)
    #         else:
    #             state_next, reward, terminal, info, turn = env.stepRed(action)
    #
    #         matrix = state_next[0]
    #         # reward = reward if not terminal else -reward
    #         state_next = np.reshape(state_next[1], [1, observation_space])
    #         results.append(state)
    #         # dqn_solver.remember(state, action[0], reward, state_next, terminal)
    #         state = state_next
    #         if step > 180:
    #             terminal = True
    #
    #         if terminal:
    #             if reward != 0:
    #                 if turn == (255, 0, 0):
    #                     blueWin = blueWin + 1
    #                     blue.append(blueWin)
    #                     red.append(redWin)
    #                     # print("blue",blue)
    #
    #                 if turn == (0, 0, 255):
    #                     redWin = redWin + 1
    #                     blue.append(blueWin)
    #                     red.append(redWin)
    #             else:
    #                 blue.append(blueWin)
    #                 red.append(redWin)
    #
    #             steps.append(step)
    #             for result in range(len(results)):
    #                 dqn_solver.remember(state, reward)
    #
    #             print("Run: " + str(run) + ", exploration: " + str(dqn_solver.exploration_rate) + ", score: " + str(
    #                 step) + " redWin " + str(redWin) + "  blueWin " + str(blueWin))
    #             break
    #         else:
    #             decay = True
    #             dqn_solver.experience_replay(decay)
    #
    #     if run % 100 == 0:
    #         dqn_solver.model.save_weights(checkpoint_fname, overwrite=True)
    #         print("model saved")
    #         if os.path.exists(checkpoint_fname):
    #             dqn_solver.model.load_weights(checkpoint_fname)
    #             print("Restored")
    #         else:
    #             print("Training fresh model")
    # print(blue)
    # print(red)
    # print(game)
    # print(moves)
    #
    # if os.path.exists(checkpoint_fname):
    #     dqn_solver.model.load_weights(checkpoint_fname)
    #     print("Restored")
    # else:
    #     print("Training fresh model")

    while run <1201:
        run += 1
        game.append(run)
        state = env.reset()
        matrix = state[0]
        state = np.reshape(state[1], [1, observation_space])
        step = 0
        turn = (0, 0, 255)

        while True:
            ep = False
            step += 1
            act1 = dqn_solver.act(state, matrix, turn, ep)
            action = act1, matrix
            state_next, reward, terminal, info, turn = env.step(action)
            if reward == 1:
                blueWin = blueWin + 1
                blue.append(blueWin)
                red.append(redWin)

            if reward == -1:
                redWin = redWin + 1
                blue.append(blueWin)
                red.append(redWin)
            if step > 150:
                terminal = True
                red.append(redWin)
                blue.append(blueWin)

            matrix = state_next[0]
            state_next = np.reshape(state_next[1], [1, observation_space])
            # dqn_solver.remember(state, reward)
            state = state_next
            if terminal:
                steps.append(step)
                # print("steps",steps)

                print("Run: " + str(run) + ", exploration: " + str(dqn_solver.exploration_rate) + ", score: " + str(
                    step) + " redWin " + str(redWin) + "  blueWin " + str(blueWin))
                break
            # else:
            #     dqn_solver.experience_replay()
    return game, red, blue, steps


if __name__ == "__main__":
    game, redWin, blueWin, moves = checkers()
    print("game", game)
    print("red", redWin)
    print("blue", blueWin)
    print("moves", moves)
    percentBlue = []
    percentRed = []
    games = []
    percentBlueWin = 0
    percentRedWin = 0
    blueWin1 = 0
    blueWin2 = 0
    redWin1 = 0
    redWin2 = 0
    # games.append(0)
    # percentBlue.append(0)
    # percentRed.append(0)
    for i in range(len(game)):
        if i % 100 == 1:
            blueWin1 = blueWin[i]
            redWin1 = redWin[i]
        if i % 100 == 0:
            blueWin2 = blueWin[i]
            redWin2 = redWin[i]
            games.append(i)
            percentBlueWin = ((blueWin2 - blueWin1) / 100) * 100
            percentRedWin = ((redWin2 - redWin1) / 100) * 100
            percentBlue.append(percentBlueWin)
            percentRed.append(percentRedWin)

    print(games)
    print(percentBlue)
    print(percentRed)

    plt.plot(games, percentRed, "r-")
    plt.plot(games, percentBlue, "blue")

    plt.xlabel('No. of Game')
    plt.ylabel('Percentage of wins ')
    plt.show()

    # plt.axis([0, , 0, 3000])
    plt.plot(game, redWin, "r-")
    plt.plot(game, blueWin, "blue")

    plt.xlabel('Game')
    plt.ylabel('Wins or loses ')
    plt.show()

    plt.plot(game, moves, "r-")

    plt.xlabel('Game')
    plt.ylabel('Steps Taken')
    plt.show()
